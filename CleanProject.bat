:: cd twice to get inside the root folder ::
cd ..
cd ..

:: Delete everything in root folder and all the plugins folders ::
SET mypath=%~dp0
@RD /S /Q "Binaries"
@RD /S /Q "Saved"
@RD /S /Q "Intermediate"
FOR /D %%d IN (Plugins\*) DO (@RD /S /Q "%%d/Binaries" )
FOR /D %%d IN (Plugins\*) DO (@RD /S /Q "%%d/Intermediate" )

:: Recreate code ::
"D:/Program Files/Epic Games/UE_4.21/Engine/Binaries/DotNET/UnrealBuildTool.exe" -projectfiles -project="E:/Projects/CodePlugins/CodePlugins.uproject" -game -rocket -progress
::"D:\Program Files\Epic Games\UE_4.21\Engine\Build\BatchFiles\Build.bat" CodePluginSampleEditor Win64 Development "E:\Projects\CodePluginSample\CodePluginSample.uproject" -WaitMutex -FromMsBuild

:: Start the project ::
start E:/Projects/CodePlugins/CodePlugins.uproject

pause
