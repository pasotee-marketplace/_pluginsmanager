@ECHO Off 
:: Go to root plugins folder and then to selected folder::

cd ..
cd %selectedDir%/

setlocal enabledelayedexpansion

if NOT exist Zips md Zips

for %%a in (%list%) do (

	echo Packing %selectedDir% version %version% for %%a
	
	:: Get the uplugin file::
	set textFile=%selectedDir%.uplugin

	:: FIND AND REPLACE ENGINE AND PLUGIN VERSIONS ::
	for /f "delims=" %%j in ('type "!textFile!" ^& break ^> "!textFile!" ') do (
		set "line=%%j"
		set line=!line:versionengine=%%a!
		set line=!line:versionplugin=%version%!
		echo !line!>>!textFile!
	)
	
	setlocal enabledelayedexpansion
	:: CREATE THE ZIP FILE ::
	%zipPath% a -afzip -m5 -ed -r -ep1 Zips/%selectedDir%%version%-%%a.zip . -xBinaries -xIntermediate -xZips -x.git -x.gitignore -x*\.git -x*\.git\*
	
	::REPLACE THE ENGINE AND PLUGIN BACK TO ORIGINAL ::
	for /f "delims=" %%j in ('type "!textFile!" ^& break ^> "!textFile!" ') do (
		set "line=%%j"
		set line=!line:%%a=versionengine!
		set line=!line:%version%=versionplugin!
		echo !line!>>!textFile!
	)
)

%zipPath% a -afzip -m5 -ed -r -ep1 Zips/%selectedDir%%version%.zip Zips/%selectedDir%%version%*

echo. && echo Packing done. && echo.

cd ..
cd _PluginsManager/