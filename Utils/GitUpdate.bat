cd ..
cd %selectedDir%/

git checkout Live
git merge Dev
cd wiki

ECHO #Version %version%# > tmp.txt
ECHO %UpdateNots% >> tmp.txt
ECHO.>> tmp.txt
type "Home.md" >> tmp.txt
type tmp.txt > "Home.md"
del tmp.txt

:: Commit Readme update on wiki git ::
git add .
git commit -m "Version %version%"
git push

:: Commit actual data on live ::
cd ..
git add .
git commit -m "Version %version%"
git push

cd ..
cd _PluginsManager/