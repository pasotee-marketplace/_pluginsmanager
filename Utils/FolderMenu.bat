@ECHO Off 

:: Get the current directory Name ::
for %%I in (.) do set CurrDirName=%%~nxI

:: cd back into plugins folder ::
cd ..

:: CALCULATE CURRENT PATH ::
set currentPath=%cd%

:: CREATE MENU OPTIONS ::
setlocal enabledelayedexpansion
set /A Counter=0

echo. && echo Menu && echo.

for /d %%f in (%currentPath%\*) do (
	for %%I in (%%f) do set localDir=%%~nxI
    	if NOT "!localDir!" == "%CurrDirName%" echo Press !Counter! for !localDir! & set /A Counter+=1
)

echo. && echo.
endlocal

:: READ INPUT CHOICE ::
set /p choiceNumber=Enter the number:

setlocal enabledelayedexpansion
set /A Counter=0
for /d %%f in (%currentPath%\*) do (
	for %%I in (%%f) do set localDir=%%~nxI
    if !Counter! == %choiceNumber% set choiceDir=!localDir!
    if NOT "!localDir!" == "%CurrDirName%" set /A Counter+=1
)

endlocal & set "selectedDir=%choiceDir%"

:: cd back to main folder::
cd _PluginsManager/

